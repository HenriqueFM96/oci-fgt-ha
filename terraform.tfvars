// Place your OCI credentials here. To have it easily, create a new API key under your user account
tenancy_ocid     = "ocid1.tenancy.oc1..aaaaaaaambr3uzztoyhweohbzqqdo775h7d3t54zpmzkp4b2cf35vs55ck3a"
compartment_ocid = "ocid1.compartment.oc1..aaaaaaaaanjxtgy5g7zhktonu3zbqb2tdhmsxjepxtucfsmcatisajzcvdza"
user_ocid        = "b2ea0e29fd1846ce900aebdf045fb267"
fingerprint      = "57:b3:c5:c3:25:98:ef:c5:d7:ca:9a:e7:3d:dd:1c:b4"
private_key_path = "./ssh-key-corporate.pem"
// you can have a full list of regions-identifiers here: https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm
region           = "sa-saopaulo-1"
